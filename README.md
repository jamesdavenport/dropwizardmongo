# Project Purpose

This project is a Dropwizard instance that is configured to use MongoDB as the database and enables database migration similar to the db migrate native to Dropwizard. cf. [https://www.dropwizard.io/en/latest/manual/migrations.html](https://www.dropwizard.io/en/latest/manual/migrations.html)

## Scope
The migration functionality stems from  use of third party jars that are readily available on the open internet. This projects successfully configures use of these jars within a Dropwizard context and then accepts a command of the form `mongo migrate`. 

## Files
Other than the standard Dropwizard project structure. You'll see a change set in resources/migrations/changeset1.xml. This is analogous to having a migrations.xml and uses the standard `db migrate` command. You can see full configuration in DropwizardMongoDBApplication.java and DropwizardMongoDBConfiguration.java. 


## Use
This project relies on having a suitable environment to run in. This includes a MongoDB instance running. Once this is configured you can run the the server in the usual way with the command

`java -jar target/dropwizardMongoDB-1.0.0.jar server src/main/resources/dropwizardmongodb.yml`

To run a database migration with a "mongo" command, do it like this

`java -jar target/dropwizardMongoDB-1.0.0.jar  mongo migrate  dropwizardmongodb.yml`

## Retrofitting an Existing Dropwizard Project

You can now add the migration capability to an existing Dropwizard service. You just need to 
* update your pom suitably ; e.g. see pom.xml here. 
* Setup a proposed migration in resources/migrations/changeset1.xml
* Follow the configuration of DropwizardMongoDBApplication.java and DropwizardMongoDBConfiguration.java.
