package com.mongo.migrate.example;



import com.mongo.migrate.example.resource.EmployeeResource;
import com.mongo.migrate.example.service.MongoService;
import com.commercehub.dropwizard.mongeez.MongeezBundle;
import com.commercehub.dropwizard.mongeez.MongoMigrateTask;
import com.commercehub.dropwizard.mongo.MongoClientFactory;
import com.commercehub.dropwizard.mongo.MongoConfiguration;
import com.mongo.migrate.example.DropwizardMongoDBConfiguration;
import com.mongo.migrate.example.MongoManaged;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.servlets.tasks.Task;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.bson.Document;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;


public class DropwizardMongoDBApplication extends Application<DropwizardMongoDBConfiguration> {

    private static final Logger logger = LoggerFactory.getLogger(DropwizardMongoDBApplication.class);

    public static void main(String[] args) throws Exception {
        new DropwizardMongoDBApplication().run(args);
    }


    @Override
    public void initialize(Bootstrap<DropwizardMongoDBConfiguration> b) {
        b.addBundle(new MongeezBundle<DropwizardMongoDBConfiguration>() {

            @Override
            public MongoClientFactory getMongoClientFactory(DropwizardMongoDBConfiguration config) {
                return config.getMongo();
            }

        });
    }



    @Override
    public void run(DropwizardMongoDBConfiguration config, Environment env)
            throws Exception {


        MongoConfiguration<DropwizardMongoDBConfiguration> mongoConfig = new MongoConfiguration<DropwizardMongoDBConfiguration>() {

            @Override
            public MongoClientFactory getMongoClientFactory(DropwizardMongoDBConfiguration configuration) {
                return configuration.getMongo();
            }
        };


        Task migrateTask = new MongoMigrateTask<>("mongoMigrate", mongoConfig, config);
        env.admin().addTask(migrateTask);




        MongoClient mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());
        MongoManaged mongoManaged = new MongoManaged(mongoClient);
        env.lifecycle().manage(mongoManaged);
        MongoDatabase db = mongoClient.getDatabase(config.getMongoDB());
        MongoCollection<Document> collection = db.getCollection(config.getCollectionName());
        logger.info("Registering RESTful API resources");

        Map<String, Object> properties = new HashMap<>();
        properties.put(ServerProperties.WADL_FEATURE_DISABLE, false);
        env.jersey().getResourceConfig().addProperties(properties);

        //env.jersey().register(new PingResource());
        env.jersey().register(new EmployeeResource(collection, new MongoService()));
//        env.healthChecks().register("DropwizardMongoDBHealthCheck",
//                new DropwizardMongoDBHealthCheckResource(mongoClient));
    }
}
